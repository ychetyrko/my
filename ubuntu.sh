
# (!) IMPORTANT (!) REQUIRED (!)
# /etc/sysctl.conf:
# net.ipv4.ip_forward = 1

sudo iptables -A INPUT

# Common
sudo apt update
sudo apt upgrade

# Repo: Docker Engine
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# NodeJS Repository
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -

# Repo: GitLab Runner
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

# Repo: .NET Core
wget https://packages.microsoft.com/config/ubuntu/22.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb

# Utils, docker, gitlab-runner, juju, kubectl
sudo apt install -y selinux-utils nginx glances ncdu iotop htop
sudo apt install -y docker-ce docker-ce-cli docker-compose-plugin
sudo apt install -y gitlab-runner
sudo apt install snapd
sudo snap install juju --classic
sudo snap install kubectl --classic

# Permissions
sudo usermod -a -G lxd $(whoami)
sudo usermod -a -G docker $(whoami)
sudo usermod -a -G docker gitlab-runner

# Initialize LXD
# sudo mkdir -v -p /mnt/hdd/storage/lxd
# sudo mkdir -v -p /var/snap/lxd/common/lxd
# sudo mount -v --bind /mnt/hdd/storage/lxd /var/snap/lxd/common/lxd
sudo lxd init --auto --storage-backend=dir
lxc network set lxdbr0 ipv6.address none
lxc network set lxdbr0 ipv6.firewall false
lxc network set lxdbr0 ipv6.nat false
